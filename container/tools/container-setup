#!/bin/bash
# After cloning down the repo/version with setup-containerized-canesm, this script gets source for version specific
# considerations

#~~~~~~~~~~~~~~~
# Functions/Trap
#~~~~~~~~~~~~~~~
trap 'cleanup $?' ERR
function cleanup(){
    local EXIT_STATUS
    EXIT_STATUS=$1
    echo "Error occurred in the setup! Exit status=$EXIT_STATUS"
    cd $CURRENT_WORKING_DIRECTORY
    rm -rf $WRK_DIR
    exit $EXIT_STATUS
}

function bail(){
    echo "********************"
    echo "ERROR:"
    echo "  $1"
    echo "********************"
    exit 1
}

#~~~~~~~
# Main
#~~~~~~~
[[ -z "$RUNID" ]]       && bail "The setup script needs a runid!"
[[ -z "$WRK_DIR" ]]     && bail "The setup script needs to know location of the run directory!"

# build necessary storage directories
run_config_dir="${WRK_DIR}/config"
run_bin_dir="${WRK_DIR}/bin"
run_output_dir="${WRK_DIR}/output"
mkdir $run_config_dir    # where namelists/compilation templates/cpp files should be placed
mkdir $run_bin_dir       # where the executables will stored (once compiled)
mkdir $run_output_dir

# link necessary tools to bin directory
ln -s ${WRK_DIR}/canesm/CCCma_tools/container/tools/config-containerized-canesm ${run_bin_dir}/config-containerized-canesm

# get main config file and perform simple replacements
run_config_template="${WRK_DIR}/canesm/CCCma_tools/container/tools/config/canesm.cfg"
cp $run_config_template .
misc_var_regex='\S*\s\?' # match any number of non-whitespace chars and then 0/1 white space character
sed -i "s#RUNID=${misc_var_regex}#RUNID=$RUNID#g" canesm.cfg
sed -i "s#WRK_DIR=${misc_var_regex}#WRK_DIR=$WRK_DIR#g" canesm.cfg
echo "Run Directory setup!"
echo "Navigate into ${RUNID}, edit canesm.cfg, and then run bin/config-canesm to setup the necessary config files"

#!/bin/bash
###
# Converts dk files to .F files and cdk to .h using up2cpp

# This script assumes that all .cdk files become .h files. xit2.cdk seems to be the only exception
CDK_TO_DK=xit2.cdk
for f in $CDK_TO_DK; do
    if [ -f "$f" ]; then
        mv $f ${f%.*}.cdk
    fi
done

for f in $(ls *.dk); do
    up2cpp $f --out_file=${f%.*}.F
done
for f in $(ls *.cdk); do
    up2cpp $f --out_file=${f%.*}.h
done

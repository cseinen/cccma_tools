runid       restart_files   restart_dates   pp_rdm_num_pert canesm_cfg:runmode  namelist_mods:PHYS_PARM:pp_solar_const namelist_mods:PHYS_PARM:ap_drngat namelist_mods:modl.dat:agcm_rstctl
run-001     mc_abc          4500_m12	        0               AMIP-nudged                     1360.747                        0.08                                    0
run-002     mc_abc          4550_m12	        2               AMIP-nudged                     1361.747                        0.09                                    1
run-003     mc_abc          4600_m12	        4               AMIP-nudged                     1362.747                        0.1                                     0
run-004     mc_abc          4650_m12	        6               AMIP-nudged                     1363.747                        0.11                                    1
run-005     mc_abc          4700_m12	        8               AMIP-nudged                     1364.747                        0.12                                    0

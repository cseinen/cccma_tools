import shutil
import os
import sys

if len(sys.argv) > 1:
    if sys.argv[1] == 'dev':
        folder = r'/fs/home/fs1/eccc/crd/ccrn_shr/scrd104/public_html/documentation/canesm-ensemble-dev'
    else:
        folder = r'/fs/home/fs1/eccc/crd/ccrn_shr/scrd104/public_html/documentation/canesm-ensemble'
else:
    folder = r'/fs/home/fs1/eccc/crd/ccrn_shr/scrd104/public_html/documentation/canesm-ensemble'

try:
    print('cleaning doc tree...')
    shutil.rmtree(folder)
    # os.rmdir(folder)  # fails due to file permissions
except Exception as e:
    print(e)

try:
    print('making documentation folder...')
    os.makedirs(folder)
except Exception as e:
    print(e)


print('moving docs to public folder...')
src = os.path.join('docs', 'build')
for file in os.listdir(src):
    print('moving ' + file + '...')
    shutil.move(os.path.join(src, file), os.path.join(folder, file))

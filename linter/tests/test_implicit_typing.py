
import sys
import os
lint_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(f'{lint_dir}/../')
from lib.linter import check_implicit_typing

def test_function_implicit_real():
  """Check to make sure that implicit real are caught in functions
  """

  source = []
  source.append('function foo()')
  source.append('implicit real')
  source.append('end function foo')
  assert check_implicit_typing(source)

def test_function_implicit_multiple():
  """Check to make sure that implicit real are caught in functions
  """

  source = []
  source.append('function foo()')
  source.append('implicit real (a-c), integer(d-e), logical f, complex g, character (o-p)')
  source.append('end function foo')
  assert check_implicit_typing(source)

def test_function_implicit_none():
  """Check to make sure that implicit real are caught in functions
  """

  source = []
  source.append('function foo()')
  source.append('implicit none')
  source.append('end function foo')
  assert not check_implicit_typing(source)
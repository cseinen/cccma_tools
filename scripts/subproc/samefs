#! /usr/bin/perl

#    Aug 27/96 - M.Berkley (verbose option)
#    Aug 27/96 - F.Majaess (cosmetic changes)
#    
#id  samefs  - Used to check if two pathes belong on the same filesystem.
#
#    AUTHOR  - Peter Silva.
#
#  PURPOSE - "samefs" script, given two pathes, is used to check if both
#            are on the same filesystem in which case the script exits
#            with (0) exit status.
#            Note: Appropriate message is issued as well.
#
#pr PARAMETERS:
#pr
#pr   POSITIONAL
#pr
#pr     arg1    = First  path (possibly containing filename) to check.
#pr
#pr     arg2    = Second path (possibly containing filename) to check.
#pr
#pr   PRIMARY/SECONDARY
#pr
#pr     verbose = print out a message if verbose, else use status
#pr

#ex EXAMPLE:
#ex
#ex   samefs ./ $RUNPATH
#ex
#ex     The above example returns "0" (successful) exit status if the 
#ex     current directory is residing on the same filesystem as the
#ex     directory pointed to by "RUNPATH" environment variable, 
#ex     otherwise a non (0) exit status is returned. .
#   

if ( $#ARGV < 1 ) {
   printf "expected two file names to check for identical file system residence\n";
   exit(-1);
}

$VERBOSE=0;

if ( $#ARGV > 1) {
    if ( $ARGV[2] =~ "[vV]*" ) {
	$VERBOSE=1;
    }
}

$L=$ARGV[0];
$R=$ARGV[1];

( $Ldev, $Lino, $Lmode, $Lnlink, $Luid, $Lgid, $Lrdev, $Lsize, 
	$Latime, $Lmtime, $Lctime, $Lblksize, $Lblocks ) =stat( $L );
( $Rdev, $Rino, $Rmode, $Rnlink, $Ruid, $Rgid, $Rrdev, $Rsize, 
	$Ratime, $Rmtime, $Rctime, $Rblksize, $Rblocks ) =stat( $R );

# printf "leftdev=%d, Rightdev=%d\n", $Ldev, $Rdev;
if ( $Ldev ==  $Rdev ) {
    if ( $VERBOSE ) {
	print "same file system\n";
    }
    exit(0);
} else {
    if ( $VERBOSE ) {
	print "different file system\n";
    }
    exit(1);
}

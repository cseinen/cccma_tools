#! /bin/sh

#    Jun 25/02 - F.Majaess

#id  chkfldr - Checks for reference to a file or directory.

#    AUTHOR  - S.Kharin,F.Majaess

#hd  PURPOSE - "chkfldr" script is used to check whether the first
#hd            argument specified is referencing a file or a 
#hd            subdirectory. In the event it's a subdirectory, then
#hd            the path to it is written into a local file whose 
#hd            default name may be altered by the second argument,
#hd            if specified.
#hd            Note: At least "arg1" should be specified and no
#hd                  more than 2 arguments.

#pr  PARAMETERS:
#pr
#pr    OSITIONAL
#pr
#pr      arg1   = file/link/subdirectory reference
#pr
#pr    PRIMARY
#pr
#pr      arg2   = filename to use for writing the subdiretory path.
#pr               It is deleted if present before performing the check.
#pr               Created only if a subdiretory is referenced in the 
#pr               first argument.
#pr               (=.tmp_chkfldr)

#ex  EXAMPLE:
#ex
#ex          chkfldr npakgg .tmp_dirtst
#ex
#ex Typical use of it to check whether "npakgg" is referencing a dataset
#ex or linked to a subdirectory. If it is the latter case, the path of
#ex the subdirectory pointed to by the link is written into ".tmp_dirtst"
#ex local file.
#ex

# set -x
dir_fil=".tmp_chkfldr"
if [ $# -gt 2 ] ; then
 echo " chkfldr: Sorry, a maximum of 2 arguments are allowed!"
 exit 1
elif [ $# -lt 1 ] ; then
 echo " chkfldr: Sorry, at least one argument needs to be specified!"
 exit 2
elif [ $# -eq 2 ] ; then
 dir_fil="$2" 
fi

(\rm -f "$dir_fil" || : )

if [ -d "$1/." ] ; then 

#   Write the subdirectory path pointed to by "arg1" into 
#   "dir_fil" file.

  \ls -dl "$1" | ${AWK:-awk} '{print $NF}' > "$dir_fil"

fi

#!/bin/sh

#    May 28/98 - F.Majaess (Revised to cope with "void name_...")
#    Jan 27/92 - E. Chan

#id  ccsplit - extracts C routines from a list of source files

#    AUTHOR  - E. Chan

#hd  PURPOSE - "ccsplit" extracts all C routines from a list of source files.
#hd            Each routine is saved as the name of the routine followed by
#hd            a ".c" extension. The source files may contain a mixture
#hd            of Fortran and C source code. A file called is also created
#hd            which contains just the Fortran source code. The individual
#hd            C routines and the Fortran source code files will be placed 
#hd            into the directory where the script was executed.
#hd 
#hd            The C routines must all begin with '#include' at the beginning
#hd            of the first line and end with a '}' at the beginning of the
#hd            last line. Each C routine must also have a name containing an
#hd            underscore appended at the end and there must not be any
#hd            whitespace between the underscore and the left parenthesis.
#hd            These conventions are necessary for the script to operate
#hd            correctly on the C source code (NB: the underscore is required
#hd            in all C routines that are to be called from Fortran). 

#pr  PARAMETERS:
#pr 
#pr    POSITIONAL
#pr 
#pr      fn1 fn2 ... fnm = list of m file names containing source code
#pr                        (filenames may contain path information)
#pr 
#pr    PRIMARY
#pr   
#pr                    f = name of file to contain the Fortran source code
#pr                        (='fortran.src') 

#ex  EXAMPLE:
#ex    
#ex    ccsplit srcfile1 /home/srcfile2 f=source.f
#ex      
#ex    The above example splits out all C routines from srcfile1 and 
#ex    /home/srcfile2 and generates a file called "source.f" which
#ex    contains all of the remaining Fortran code.

#  * Reset field separators (otherwise those defined in the parent process
#  * will be used and these may cause problems if they include special 
#  * characters used in this script). 

IFS=' '
export IFS

#  * Move into a temporary directory to execute the script.

mkdir -m 755 tmp$$
cd tmp$$

#  * Obtain the filename to be used for the Fortran source code file and
#  * the list of source files to be processed. 

for arg in $@ 
do
  case $arg in 
    f=*) eval $arg                        ;;
      *) file_list="$file_list $arg"          
  esac
done

#  * Set the default Fortran source code file to "fortran.src".

f=${f:=fortran.src}

#  * Loop over all source files.

for name in $file_list
do            

  #  * Extract name and path information from the source filename. The 
  #  * default directory containing the source file is set to the parent
  #  * directory. 

  path=`expr $name : '\(.*\)/'`
  srcfile=`expr //$name : '.*/\(.*\)'`

  path=${path:=..}

  #  * Invoke sed to separate the C routines from the Fortran source code.
  #  * The C routines are placed in a temporary file and the Fortran 
  #  * source code is stored in the file "fortran.src". The C routines 
  #  * must begin with '#include' and end with '}'.

  sed "

  /^#include/,/^}/ {
  w tmpfile$$
  d
  }
 
  " "$path/$srcfile" >> $f

  #  * If C source file is not empty, split the C routines into separate 
  #  * files. The files are named using the name of the routine with the 
  #  * extension ".c" appended at the end. These files are placed in the 
  #  * parent directory.

  if [ -s tmpfile$$ ] ; then

    csplit -s -k tmpfile$$ '/^}/+1' {99} 2>/dev/null
    rm -f `ls xx* | tail -1`

    for file in xx*  
    do 
      line=`grep '_(' $file`
#     cfile=`expr "$line" : '\(.*\)_'`
      cfile=`expr " $line" : '.* \(.*\)_'`
      mv $file ../${cfile}.c 
    done

  fi

  #  * Remove temporary file before processing next source file.

  rm -f tmpfile$$ 

done

#  * Move Fortran source file into parent directory if file is not empty.

if [ -s "$f" ] ; then
  mv $f ..
fi

#  * Move back into parent directory and clean up temporary directory
#  * before exiting from script.

cd ..
rm -r tmp$$

exit




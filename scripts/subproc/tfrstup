#    SEP 06/17 - F.Majaess
 
#id tfrstup - Used for handling needed data transfer setup
 
#   AUTHOR  - F.Majaess
 
#hd PURPOSE - "tfrstup" script is used in the process of
#hd            handling data transfer tasks across platforms. 
#hd            That may include defining functions, creating 
#hd            subdirectories, files, ...etc.
#hd            NOTE: This script is not intended to be called
#hd                  directly, rather indirectly by other 
#hd                  higher level scripts expected executing
#hd                  from within xfer queue on XC40 I/O node.
#hd 
 
# set +x

#  * Set variable 'AWK' to the version of "awk" to be used.
 
AWK=${AWK:='awk'}
HOSTID=${HOSTID:=`hostname| cut -d'.' -f1 `}
HOSTID=`echo $HOSTID | sed -e 's/hpcr4-in/ppp4/' -e 's/hpcr3-in/ppp3/' `
HOSTIDf=`echo $HOSTID | sed -e 's/eccc.*-ppp/cs/g' -e 's/^ppp/cs/g' | cut -c 1-3`

# Ensure valid host and DESTSSPATH destination site store directory settings.

if [ "$HOSTIDf" = 'xc4' ] ; then
 if [ -n "$DESTSSPATH" ] ; then
  HALL=`echo $DESTSSPATH | sed -e 's/^.*\/hall/hall/' | sed -e 's/\/.*$//'`
  SiTeStOr=`echo $DESTSSPATH | sed -e 's/^.*\/sitestore\/.*$/sitestore/'`
  SiTeStOr=${SiTeStOr:-'fake'}
  if [ "$SiTeStOr" != 'sitestore' -o "$HALL" != 'hall4' ] ; then
   echo " tfrstup - Invalid setting of DESTSSPATH=$DESTSSPATH !"  
   touch haltit
   echo " tfrstup - Invalid setting of DESTSSPATH=$DESTSSPATH !" >> haltit
   abort='abort'
   exit 1
  fi
 else
  # DESTSSPATH=`echo $RUNPATH_USER | sed -e 's/\/hall4\/work\//\/hall4\/sitestore\//'`
  DESTSSPATH=`echo $RUNPATH | sed -e 's/\/hall4\/work\//\/hall4\/sitestore\//'`
 fi
 trgetfe='eccc-ppp4'
elif [ "$HOSTIDf" = 'xc3' ] ; then
 if [ -n "$DESTSSPATH" ] ; then
  HALL=`echo $DESTSSPATH | sed -e 's/^.*\/hall/hall/' | sed -e 's/\/.*$//'`
  SiTeStOr=`echo $DESTSSPATH | sed -e 's/^.*\/sitestore\/.*$/sitestore/'`
  SiTeStOr=${SiTeStOr:-'fake'}
  if [ "$SiTeStOr" != 'sitestore' -o "$HALL" != 'hall3' ] ; then
   echo " tfrstup - Invalid setting of DESTSSPATH=$DESTSSPATH !"  
   touch haltit
   echo " tfrstup - Invalid setting of DESTSSPATH=$DESTSSPATH !" >> haltit
   abort='abort'
   exit 1
  fi
 else
  # DESTSSPATH=`echo $RUNPATH_USER | sed -e 's/\/hall3\/work\//\/hall3\/sitestore\//'`
  DESTSSPATH=`echo $RUNPATH | sed -e 's/\/hall3\/work\//\/hall3\/sitestore\//'`
 fi
 trgetfe='eccc-ppp3'
else
 echo " tfrstup - Sorry local $HOSTID host is not supported for this task"
 touch haltit
 echo " tfrstup - Sorry local $HOSTID host is not supported for this task" >> haltit
 abort='abort'
 exit 1
fi
if [ ! -d "${DESTSSPATH}/." ] ; then
 echo " tfrstup - Sorry ${DESTSSPATH} is not accessible from $HOSTID host !"
 touch haltit
 echo " tfrstup - Sorry ${DESTSSPATH} is not accessible from $HOSTID host !" >> haltit
 abort='abort'
 exit 1
fi

destsspath="${DESTSSPATH}/tmp.t.${invoker_deck}_${host}_${nqsprfx}${jobname}${nqsext}_`date +%Y%j%H%M%S`"
(rm -f .fe_save.sh .be2fe_cp.sh .be_delete.sh || : )
if [ -n "$CUSTOM_ENV_FILE" ] ; then
 GenEnvFil=". $CUSTOM_ENV_FILE"
else
 GenEnvFil=' : '
fi
echo "#! /bin/bash
$GenEnvFil &&
cd $destsspath/. &&
RUNPATH=$DESTSSPATH &&
export RUNPATH &&" > .fe_save.sh
chmod u+x .fe_save.sh

echo "mkdir -m 1755 ${destsspath} &&" > .be2fe_cp.sh

if [ "$delete_files" = 'yes' ] ; then
 touch .be_delete.sh 
fi

## Define functions to be called upon.

be2fecp () {
 if [ "$abort" != 'abort' ] ; then
  echo '' ; echo " tfrstup,be2fecp : Attempt copying the files into ${destsspath}" ; echo ''
  if [ -s ./.be2fe_cp.sh ] ; then
   echo " : || abort='abort' " >> ./.be2fe_cp.sh
   echo ' [ "$abort" = abort ] && exit 1 || :' >> ./.be2fe_cp.sh 
   echo " ------ Contents of .be2fe_cp.sh: " ; echo '---------' ; cat .be2fe_cp.sh ; echo '---------' ; echo ''
   . .be2fe_cp.sh
  else
   echo " tfrstup,be2fecp - missing .be2fe_cp.sh file!"
   touch haltit
   echo " tfrstup,be2fecp - missing .be2fe_cp.sh file!" >> haltit
   abort='abort'
   exit 1
  fi 
 else
  echo " tfrstup,be2fecp - Can not proceed since abort=$abort !" 
  touch haltit
  echo " tfrstup,be2fecp - Can not proceed since abort=$abort !" >> haltit
  exit 1
 fi
 }

fesave () {
 if [ "$abort" != 'abort' ] ; then
  echo '' ; echo " tfrstup,fesave: Attempt saving the files in ${destsspath} on $trgetfe" ; echo ''
  if [ -s ./.fe_save.sh ] ; then
   echo " touch ${destsspath}/outcome_good || touch ${destsspath}/outcome_bad "  >> ./.fe_save.sh
   echo " [ -f ${destsspath}/outcome_bad ] && exit 1 || : "  >> ./.fe_save.sh
   echo " ------ Contents of .fe_save.sh: " ; echo '---------' ; cat .fe_save.sh ; echo '---------' ; echo ''
   cp -p ./.fe_save.sh ${destsspath}/.fe_save.sh &&
   ssh $trgetfe "${destsspath}/.fe_save.sh" 
  else
   echo " tfrstup,fesave - missing .fe_save.sh file!" &&
   touch haltit &&
   echo " tfrstup,fesave - missing .fe_save.sh file!" >> haltit &&
   abort='abort' &&
   (exit 1)
  fi || exit $?
 else
  echo " tfrstup,fesave - Can not proceed since abort=$abort !"
  touch haltit
  echo " tfrstup,fesave - Can not proceed since abort=$abort !" >> haltit
  exit 1
 fi
 }

becleanup () {
 if [ "$abort" != 'abort' ] ; then
  echo ''
  ls -ld ${destsspath}/outcome_*
  echo '' ; echo " ------ Contents of ${destsspath} :"
  ls -al ${destsspath}
  echo '---------' ; echo ''
  if [ -f "${destsspath}/outcome_bad" ] ; then
   echo " tfrstup,becleanup: Problem encountered in saving the files !"
   touch haltit
   echo " tfrstup,becleanup: Problem encountered in saving the files !" >> haltit
   abort='abort'
   exit 1
  else
   if [ -f "${destsspath}/outcome_good" ] ; then
    (\rm -rf ${destsspath} || : )
    if [ "$delete_files" = 'yes' -a -s ./.be_delete.sh ] ; then
     echo '' ; echo " ------ Contents of .be_delete.sh: " ; echo '---------' ; cat .be_delete.sh ; echo '---------' ; echo ''
     . ./.be_delete.sh
    fi
   fi
  fi
 else
  echo " tfrstup,becleanup: Can not proceed since abort=$abort !"
  touch haltit
  echo " tfrstup,becleanup: Can not proceed since abort=$abort !" >> haltit
  exit 1
 fi
 }
 
putfile () {
 Ists=9 &&
 if test "$abort" != 'abort' ; then
 #set +x &&
  unset pdn &&
  eval "$@" &&
  if [ -z "$pdn" ] ; then
   echo "tfrstup,putfile: Error, missing or invalid filename for pdn parameter " &&
   touch haltit &&
   echo "tfrstup,putfile: Error, missing or invalid filename for pdn parameter " >> haltit &&
   abort=abort &&
   exit 1 
  fi &&
  pdn=`echo $pdn | tr '\*' '_'` &&
  Updn=`echo $pdn | tr '[a-z]' '[A-Z]'` &&
  lpdn=`echo $pdn | tr '[A-Z]' '[a-z]'` &&
  echo "" && echo "  putfile: processing $Updn" &&
  access $Updn $lpdn nocp &&
  echo "cp -Lrp $Updn ${destsspath}/$Updn && chmod u+w ${destsspath}/$Updn && " >> ./.be2fe_cp.sh &&
  echo "save $Updn $lpdn &&" >> ./.fe_save.sh &&
  if [ "$delete_files" = 'yes' ] ; then
   echo "delete $Updn " >> ./.be_delete.sh
  fi &&
  unset Taperr_msg && Ists=0 || abort='abort' 
 fi  
 [ "$abort" = 'abort' ] && ( exit 1) || :
 } 

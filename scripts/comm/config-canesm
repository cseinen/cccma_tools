#!/bin/bash
set -e
# Executable script that takes in the canesm.cfg file for a given run
# and produces a config/ directory that contains many input files to
# be used by other processes throughout the CanESM pipeline, eg:
#   - namelists
#   - cppkey files
#   - shell parameters files
#   - compilation templates
#   - runtime/compilation environment files
#
# Functions are defined at the top. See code below MAIN PROGRAM for how they are called

THIS_SCRIPT_PATH=$(type $0|awk '{print $3}') # pathname of this script
THIS_SCRIPT_NAME=$(basename $THIS_SCRIPT_PATH)
#~~~~~~~~~~
# Functions
#~~~~~~~~~~
function get_platform_config(){
    # get appropriate platform config files
    local platform_cfg
    local src_root
    local platform_name
    local compiler
    src_root=$1
    platform_name=$2
    compiler=$3
    platform_config_dir=${src_root}/CONFIG/PLATFORM/${platform_name}
    if ! is_directory ${platform_config_dir}; then
        echo "$platform_config_dir doesn't exist!"
        bail "Does a platform config directory exist for $platform_name in $src_root?"
    fi
    cp ${platform_config_dir}/*_environment .
    cp ${platform_config_dir}/*.${COMPILER}* .
}

function get_canesm_config_params(){
    # function to pull all evaluated variables from a canesm.cfg type file
    # and store them in a new shell parameter file with zero logic
    local conf_file
    local output_file
    local vars_to_ignore
    conf_file=$1
    output_file=$2

    # check that the config file exists - note that this function can't use functions
    #   from the shell functions module as the source directory isn't known yet.
    if [[ ! -f "$conf_file" ]]; then
        echo "config-canesm - get_canesm_config_params: $conf_file doesn't exist!"
        exit 1
    fi

    # create pattern of vars to ignore
    vars_to_ignore="LINES COLUMNS"  # window size variables
    ignore_pattern=""
    for var in $vars_to_ignore; do
        # add word boundaries to variables
        ignore_pattern="${ignore_pattern} \<${var}\>"
    done
    # add OR condition between variables
    ignore_pattern=$(echo $ignore_pattern | sed "s# #\\\|#")

    # get variables before configuration file is sourced
    ( set -o posix ; set ) > .tmp.variables.before.config.file

    # source config file, then get variables after
    source $conf_file
    ( set -o posix ; set ) > .tmp.variables.after.config.file

    # get differences
    diff .tmp.variables.before.config.file .tmp.variables.after.config.file |
        grep "^>\s*\S\+=" |
        sed "s/^>//" > $output_file

    rm .tmp.variables.*.config.file
}

function determine_modl_dat_to_extract(){
    local modl_dat_to_extract
    case $runmode in
                    *MAM*) modl_dat_to_extract="modl.dat_MAM" ;;
         CanESM-chem-init) modl_dat_to_extract="modl.dat_canesm_chem_init" ;;
                CanAM-PAM) modl_dat_to_extract="modl.dat_PAM" ;;
                        *) modl_dat_to_extract="modl.dat" ;;
    esac

    # send value back to calling script
    echo $modl_dat_to_extract
}

function config_agcm_modl_dat(){
    local NRFP

    # determine the number of radiative forcing calls for the
    # radiative forcing scenario
    case $RF_SCENARIO in
        SRESA1B|SRESA2|SRESB1) NRFP=1 ;;
                        2XCO2) NRFP=1 ;;
                      2X4XCO2) NRFP=2 ;;
                   4XCO2_GHAN) NRFP=2 ;;
                       CFMIPA) NRFP=3 ;;
                       AERORF) NRFP=6 ;;
                        PAMRF) NRFP=6 ;;
                            *) NRFP=1 ;;
    esac

    # make the modifications - everything beyond the above set vars should
    #   come in through the config file (canesm.cfg)
    mod_nl modl.dat ntld            \
                    ntlk            \
                    ntwt            \
                    initpool        \
                    lndcvrmod       \
                    lndcvr_offset   \
                    ilev            \
                    levs            \
                    lmt             \
                    lonsld          \
                    nlatd           \
                    lond            \
                    lonsl           \
                    nlat            \
                    lon             \
                    ntrac           \
                    ntraca          \
                    ioztyp          \
                    ioxtyp          \
                    iyear           \
                    ilaun           \
                    nnode_a         \
                    NRFP            \
                    delt            \
                    ksteps          \
                    isbeg           \
                    israd           \
                    issp            \
                    isgg            \
                    istr            \
                    icsw            \
                    iclw            \
                    ishf            \
                    isst >> ${mod_nl_log_file} 2>&1

    mod_nl modl.dat em_aerosol_scn_mode \
                    em_co2_scn_mode     \
                    conc_ozone_scn_mode \
                    vtau_scn_mode       \
                    vtaufile_type       \
                    vtau_offset_year    \
                    ghg_co2_scn_mode    \
                    ghg_ch4_scn_mode    \
                    ghg_n2o_scn_mode    \
                    ghg_f11_scn_mode    \
                    ghg_f12_scn_mode    \
                    ghg_f113_scn_mode   \
                    ghg_f114_scn_mode   \
                    ghg_co2_scn_offset  \
                    ghg_ch4_scn_offset  \
                    ghg_n2o_scn_offset  \
                    ghg_f11_scn_offset  \
                    ghg_f12_scn_offset  \
                    ghg_f113_scn_offset \
                    ghg_f114_scn_offset >> ${mod_nl_log_file} 2>&1
}

function config_agcm_RF_RTP(){
    local rf_scenario
    rf_scenario=$RF_SCENARIO
    mod_nl RF_RTP   rf_scenario \
                    trop_idx \
                    start_year_r >> ${mod_nl_log_file} 2>&1
}

function get_and_config_agcm_namelists(){
    local namelists_to_extract
    local modl_dat_to_extract
    local nl_file
    local nl_file_path
    local storage_dir
    local _namelist_storage_dir    # leading underscore to differentiate from variable in main program

    # unpack first arg as the storage directory, then shift arguments and capture the remaining as the namelists we want
    _namelist_storage_dir=${1}
    shift
    namelists_to_extract="$@"
    ! is_defined ${_namelist_storage_dir} && bail "The first argument must define the namelist storage directory!"
    ! is_defined ${namelists_to_extract}  && bail "After the first argument, you must provide a space delimited list of namelists!"
    storage_dir=${_namelist_storage_dir}/AGCM

    # get namelists
    for nl_file in $namelists_to_extract; do
        # skip if file is already present, to avoid overwritting user settings
        if is_file $nl_file; then
            echo "$nl_file present in $(pwd) - delete if you want new one extracted"
            continue
        fi

        if [[ $nl_file == "modl.dat" ]]; then
            # need special treatment for modl.dat for now
            #   This is less than ideal...
            modl_dat_to_extract=$(determine_modl_dat_to_extract)
            nl_file_path=${storage_dir}/${modl_dat_to_extract}
        else
            nl_file_path=${storage_dir}/${nl_file}
        fi

        if ! is_file $nl_file_path; then
            bail "Failed to extract $nl_file_path for the AGCM! File doesn't exist?"
        else
            cp $nl_file_path $nl_file
        fi
    done

    # configure namelists
    #   - Add more configuration functions if necessary, or we can programatically determine what config
    #     functions need to be called
    if [[ "$namelists_to_extract" == *modl.dat* ]]; then
        config_agcm_modl_dat
    fi
    if [[ "$namelists_to_extract" == *RF_RTP* ]]; then
        config_agcm_RF_RTP
    fi
}

function config_nemo_namelist_pisces(){
    local ln_c14int
    local ln_co2int
    ln_c14int=${nemo_ln_c14int:=.false.}
    ln_co2int=${nemo_ln_co2int:=.false.}
    mod_nl namelist_pisces ln_c14int ln_co2int >> ${mod_nl_log_file} 2>&1
}

function config_nemo_namelist_cfc(){
    local offset_cfc_year
    offset_cfc_year=${nemo_offset_cfc_year:=0}
    mod_nl namelist_cfc offset_cfc_year >> ${mod_nl_log_file} 2>&1
}

function config_nemo_namelist_top(){
    local ln_rsttr
    local cn_trcrst_in
    local cn_trcrst_out
    cn_trcrst_in=${nemo_cn_trcrst_in:="restart_trc_in"}
    cn_trcrst_out=${nemo_cn_trcrst_out:="restart_trc"}
    if [[ "$nemo_from_rest" == "on" ]]; then
        # starting from rest, NOT a restart
        ln_rsttr=.false.
    else
        # starting from restart
        ln_rsttr=.true.
    fi
    mod_nl namelist_top ln_rsttr _char_cn_trcrst_in _char_cn_trcrst_out >> ${mod_nl_log_file} 2>&1
}

function config_nemo_namelist_ice(){
    local cn_icerst_in
    local cn_icerst_out
    cn_icerst_in=${nemo_cn_icerst_in:="restart_ice_in"}
    cn_icerst_out=${nemo_cn_icerst_out:="restart_ice"}
    mod_nl namelist_ice _char_cn_icerst_in _char_cn_icerst_out >> ${mod_nl_log_file} 2>&1
}

function config_nemo_namelist(){
    local jpni          # number of domain decomps in 'i' direction
    local jpnj          # number of domain decomps in 'j' direction
    local jpnij         # total number of nemo tiles (jpni*jpnj)
    local rn_rdt        # dynamics time step
    local cn_exp        # experiment identifier
    local ln_rstart     # logical that tells Nemo if we are starting from rest or a restart
    local nn_rstctl     # used to define what is read from the restart vs namelist
    local cn_ocerst_in  # used to define the beginning portion of the input ocean restart name
    local cn_ocerst_out # used to define a portion of the string used to name the output restart name
    local nn_print      # controls level of printing (used in combination with nn_print)
    local nn_istate     # output initial state (1) or not (0)
    local nn_ice        # defines what ice model ise used by nemo
    local nn_fsbc       # number of nemo time steps between calls to sbc
    local nn_closea
    local nn_msh        # 0/1 (off/on) determines if nemo will create mesh file or not
    local nn_fwri
    local ln_diaptr
    local ln_diaznl
    local ln_rnf        # logical for runoff config (seems linked with ln_rnf_emp)
    local ln_rnf_emp
    local ln_tsd_init   # logical to initialize ocean T & S with input data or not
    local ln_ctl        # turns on/off additional diagnostic printing
    local ln_dimgnn
    local ln_mskland
    local ln_clobber

    # Default parameters (ones that are always configured)
    jpni=${nemo_jpni}
    jpnj=${nemo_jpnj}
    jpnij=${nemo_jpnij}
    rn_rdt=${nemo_rn_rdt}
    cn_exp=${RUNID}
    if [[ "$nemo_from_rest" == "on" ]]; then
        # starting from rest, NOT a restart
        ln_rstart=.false.
    else
        # starting from restart
        ln_rstart=.true.
    fi
    nn_rstctl=${nemo_nn_rstctl:=0}
    cn_ocerst_in=${nemo_cn_ocerst_in:="restart"}
    cn_ocerst_out=${nemo_cn_ocerst_out:="restart"}
    nn_print=${nemo_nn_print:=0}
    nn_ice=${nemo_nn_ice:=2}
    nn_fsbc=${nemo_nn_fsbc:=3}
    nn_closea=${nemo_nn_closea:=0}
    nn_istate=${nemo_nn_istate:=0}
    nn_msh=${nemo_nn_msh:=0}
    nn_fwri=${nemo_nn_fwri:=15}
    ln_diaptr=${nemo_ln_diaptr:=.false.}
    ln_diaznl=${nemo_ln_diaznl:=.true.}
    ln_ctl=${nemo_ln_ctl:=.false.}
    ln_rnf=${nemo_ln_rnf:=.true.}
    ln_rnf_emp=${nemo_ln_rnf_emp:=.true.}
    ln_tsd_init=${nemo_ln_tsd_init:=.true.}
    ln_dimgnnn=${nemo_ln_dimgnnn:=.false.}
    ln_mskland=${nemo_ln_mskland:=.false.}
    ln_clobber=${nemo_ln_clobber:=.false.}
    mod_nl namelist jpni jpnj jpnij rn_rdt _char_cn_exp ln_rstart nn_rstctl _char_cn_ocerst_in \
            _char_cn_ocerst_out ln_ctl nn_print nn_ice ln_rnf ln_rnf_emp ln_tsd_init nn_fsbc \
            nn_closea nn_msh ln_diaptr ln_diaznl nn_fwri nn_istate ln_dimgnnn ln_mskland ln_clobber >> ${mod_nl_log_file} 2>&1

    # FAFMIP namelist values
    if [[ $nemo_config == *_FAFMIP* ]]; then
        local ln_faftau
        local ln_fafemp
        local ln_fafhflx
        local ln_fafheat
        ln_faftau=${nemo_ln_faftau:=.false.}
        ln_fafemp=${nemo_ln_fafemp:=.false.}
        ln_fafhflx=${nemo_ln_fafhflx:=.true.}
        ln_fafheat=${nemo_ln_fafheat:=.false.}
        mod_nl namelist ln_faftau ln_fafemp ln_fafhflx ln_fafheat  >> ${mod_nl_log_file} 2>&1
    fi

    # Interannual forcing consideration
    if [[ $nemo_forcing == "bulk_iaf" ]]; then
        local sn_wndi=" 'uwnd10m'     ,         6         , 'U_10_MOD',   .true.    , .false. , 'yearly'  , '' , 'Uwnd' "
        local sn_wndj=" 'vwnd10m'     ,         6         , 'V_10_MOD',   .true.    , .false. , 'yearly'  , '' , 'Vwnd' "
        local sn_qsr="  'qsw'         ,        24         , 'SWDN_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_qlw="  'qlw'         ,        24         , 'LWDN_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_tair=" 'tair10m'     ,         6         , 'T_10_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_humi=" 'humi10m'     ,         6         , 'Q_10_MOD',   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_prec=" 'precip'      ,        -1         , 'PRC_MOD' ,   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_snow=" 'snow'        ,        -1         , 'SNOW'    ,   .true.    , .false. , 'yearly'  , '' , '' "
        local sn_tdif=" 'taudif_core' ,        24         , 'taudif'  ,   .false.    , .false. , 'yearly'  , '' , '' "
        local sn_rnf=" 'runoff',        -1         , 'runoff'    ,   .true.     , .false. , 'yearly'  , '', '' "
        local sn_cnf=" 'runoff',        -1         , 'socoeff'   ,   .true.     , .false. , 'yearly'  , '', '' "
        mod_nl namelist sn_wndi sn_wndj sn_qsr sn_qlw sn_tair sn_humi sn_prec sn_snow sn_tdif sn_rnf sn_cnf >> ${mod_nl_log_file} 2>&1
    fi

    if [[ $nemo_carbon == "on" ]] && [[ $nemo_trans_co2 == "on" ]]; then
        local sn_rcv_co2=" 'none' , 'no' , '' , '' , '' "
        local sn_snd_co2=" 'none' , 'no' , '' , '' , '' "
        mod_nl namelist sn_rcv_co2 sn_snd_co2 >> ${mod_nl_log_file} 2>&1
    fi
}

function get_and_config_nemo_namelists(){
    local namelists_to_extract
    local nl_file
    local nl_file_path
    local storage_dir
    local _namelist_storage_dir    # leading underscore to differentiate from variable in main program
    _namelist_storage_dir=${1}
    shift
    namelists_to_extract="$@"
    ! is_defined ${_namelist_storage_dir} && bail "The first argument must define the namelist storage directory!"
    ! is_defined ${namelists_to_extract}  && bail "After the first argument, you must provide a space delimited list of namelists!"
    storage_dir=${_namelist_storage_dir}/OCEAN/${nemo_config}/EXP00

    # get namelists
    for nl_file in $namelists_to_extract; do
        # skip if file is already present, to avoid overwritting user settings
        if is_file $nl_file; then
            echo "$nl_file present in $(pwd) - delete if you want new one extracted"
            continue
        fi

        if [[ $nl_file == "namelist_top" ]] && [[ $nemo_cfc == "on" ]]; then
            # we actually want to extract namelist_top_cfc and store it as namelist_top
            nl_file_path="${storage_dir}/namelist_top_cfc"
        else
            nl_file_path="${storage_dir}/${nl_file}"
        fi

        if ! is_file $nl_file_path; then
            bail "Failed to extract $nl_file_path for the OCEAN! File doesn't exist"
        else
            cp $nl_file_path $nl_file
        fi
    done

    # configure namelists
    #   - Add more configuration functions if necessary, or we can programatically call
    #     config* functions
    if [[ " $namelists_to_extract " == *" namelist "* ]]; then
        # note that special care to avoid flagging namelist_*
        config_nemo_namelist
    fi
    if [[ "$namelists_to_extract" == *namelist_ice* ]]; then
       config_nemo_namelist_ice
    fi
    if [[ "$namelists_to_extract" == *namelist_top* ]]; then
        config_nemo_namelist_top
    fi
    if [[ "$namelists_to_extract" == *namelist_cfc* ]]; then
        config_nemo_namelist_cfc
    fi
    if [[ "$namelists_to_extract" == *namelist_pisces* ]]; then
        config_nemo_namelist_pisces
    fi
}

function config_coupler_nl_coupler_par(){
    local env_runid
    local nlon_o
    local nlat_o

    # match variable in coupler namelist
    env_runid=$RUNID

    # determine the ocean dimensions
    case $nemo_config in
          *_ORCA1_*) nlon_o=360  ; nlat_o=292  ;;
        *_ORCA025_*) nlon_o=1440 ; nlat_o=1021 ;;
    esac

    # make the modifications
    mod_nl nl_coupler_par  windstress_remap              \
                           landfrac_bug                  \
                           cpl_rs_abort_if_missing_field \
                           bulk_in_cpl                   \
                           specified_bc_year_offset      \
                           _char_env_runid               \
                           couple_serial                 \
                           nlon_o                        \
                           nlat_o >> ${mod_nl_log_file} 2>&1
}

function get_and_config_coupler_namelists(){
    local namelists_to_extract
    local nl_file
    local nl_file_path
    local storage_dir
    local _namelist_storage_dir    # leading underscore to differentiate from variable in main program
    _namelist_storage_dir=${1}
    shift
    namelists_to_extract="$@"
    ! is_defined ${_namelist_storage_dir} && bail "The first argument must define the namelist storage directory!"
    ! is_defined ${namelists_to_extract}  && bail "After the first argument, you must provide a space delimited list of namelists!"
    storage_dir=${_namelist_storage_dir}/COUPLER

    # get namelists
    for nl_file in $namelists_to_extract; do
        # skip if file is already present, to avoid overwritting user settings
        if is_file $nl_file; then
            echo "$nl_file present in $(pwd) - delete if you want new one extracted"
            continue
        fi
        nl_file_path=${storage_dir}/${nl_file}

        if ! is_file $nl_file_path; then
            bail "Failed to extract $nl_file_path for the COUPLER! File doesn't exist"
        else
            cp $nl_file_path $nl_file
        fi
    done

    # config namelists
    #   - Add more configuration functions if necessary
    if [[ "$namelists_to_extract" == *nl_coupler_par* ]]; then
        config_coupler_nl_coupler_par
    fi
}

function generate_cpp_files(){
    local generated_cpp_sizes_file
    local generated_cpp_config_file
    local model_cpp_def_file
    local diags_cpp_def_file
    local agcm_namelist
    model_cpp_def_file=${1}
    diags_cpp_def_file=${2}
    agcm_namelist=${3}
    generated_cpp_sizes_file="cppdef_sizes.h"
    generated_cpp_config_file="cppdef_config.h"

    # generate cpp files if they don't exist
    if is_file $generated_cpp_sizes_file ; then
        echo "$generated_cpp_sizes_file already exists in 'config/'! Delete if you'd like "
        echo "a new one generated from your settings"
    else
        set_sizes ${agcm_namelist} ${generated_cpp_sizes_file}
        echo "Generated ${generated_cpp_sizes_file} in 'config/' from ${agcm_namelist}"
    fi

    if is_file $generated_cpp_config_file ; then
        echo "$generated_cpp_config_file already exists in 'config/'! Delete if you'd like "
        echo "a new one generated from your settings"
    else
        cat $model_cpp_def_file > $generated_cpp_config_file
        cat $diags_cpp_def_file >> $generated_cpp_config_file
        echo "Generated ${generated_cpp_config_file} in 'config/' from"
        echo "$model_cpp_def_file and $diags_cpp_def_file"
    fi
}

#*********************************************#
#           MAIN PROGRAM
#*********************************************#

#~~~~~~~~~~~~~~~~~
# Parse Arguments
#~~~~~~~~~~~~~~~~~
config_file=""
call_dir=$(pwd)
mod_nl_log_file="${call_dir}/.mod_nl_$$_change.log"
for arg in "$@"; do
    case $arg in
        *=*)
            var=$(echo $arg | awk -F\= '{printf "%s",$1}')
            val=$(echo $arg | awk -F\= '{printf "%s",$2}')
            case $var in
                config_file) config_file="$val" ;; # file containing high level config settings
                          *) bail "Invalid command line arg --> $arg <--" ;;
            esac ;;
          *)
            bail "Invalid command line arg --> $arg <--" ;;
    esac
done
# check that config file was provided (helper functions like bail/is_defined isn't brought in yet)
[[ -z $config_file ]] && { echo "config_file must be provided to $THIS_SCRIPT_NAME!"; exit 1;}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Get config settings and create evalulated shell params file
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
shell_params_file="canesm-shell-params.sh"
get_canesm_config_params $config_file $shell_params_file
source ${shell_params_file}

# get many useful shell functions
source ${CANESM_SRC_ROOT}/CCCma_tools/tools/CanESM_shell_functions.sh

#~~~~~~~~~~~~~~~~~~~~~~~
# Setup config directory
#~~~~~~~~~~~~~~~~~~~~~~~
run_config_dir=${WRK_DIR}/config
mkdir -p ${run_config_dir}
pushd ${run_config_dir} >> /dev/null

# get environment/compilation files
get_platform_config $CANESM_SRC_ROOT $PLATFORM $COMPILER

# make link to the basefiles
ln -sf ${CANESM_SRC_ROOT}/CCCma_tools/cccjob_dir/lib/jobdefs/${model_basefile}_jobdef basefile
ln -sf ${CANESM_SRC_ROOT}/CCCma_tools/cccjob_dir/lib/jobdefs/${diag_basefile}_jobdef diag_basefile

# make link to associated cppdef directory
ln -sfn ${CANESM_SRC_ROOT}/CONFIG/${CONFIG}/cppdefs cppdefs

# bring shell params file in
mv ${call_dir}/${shell_params_file} .

# get and configure namelists
# Note: it would be great if these functions didn't need to rely on the
#   environment, but unfortunately, many of the variables come from sourcing
#   canesm.cfg. Once we refactor the configuration system we can improve this
namelist_storage_dir=${CANESM_SRC_ROOT}/CONFIG/COMMON/namelists
mkdir -p namelists
pushd namelists >> /dev/null
case $CONFIG in
    "ESM")
        get_and_config_agcm_namelists $namelist_storage_dir $agcm_namelists
        get_and_config_nemo_namelists $namelist_storage_dir $nemo_namelists
        get_and_config_coupler_namelists $namelist_storage_dir $coupler_namelists
        ;;
    "AMIP")
        get_and_config_agcm_namelists $namelist_storage_dir $agcm_namelists
        get_and_config_coupler_namelists $namelist_storage_dir $coupler_namelists
        ;;
    "OMIP")
        get_and_config_nemo_namelists $namelist_storage_dir $nemo_namelists
        ;;
esac
popd >> /dev/null
echo ""
echo "Configured run namelists and stored in config/namelists! See $(basename $mod_nl_log_file) for configuration modifications."
echo ""

# generate cpp files (ESM/AMIP only)
if [[ "$CONFIG" == "ESM" || "$CONFIG" == "AMIP" ]]; then
    generate_cpp_files $cppdefs_file $cppdefs_diag namelists/modl.dat
fi
popd >> /dev/null

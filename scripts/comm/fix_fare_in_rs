#!/bin/sh
set -e

if [ $# -ne 2 ] ; then
  echo "Usage: fix_fare_in_rs <input_restart_file> <output_restart_file>"
  exit
fi

rs1=$1
rs2=$2
echo rs1=$rs1 rs2=$rs2

curdir=`pwd`
tmpdir=tmp.rsk.$$
mkdir -p $tmpdir
cd $tmpdir

# set a trap to remove $tmpdir on exit
trap "cleanup" 0 1 2 3 6 15
cleanup()
{
  exit_status=$?
  echo exit_status=$exit_status
  echo "Caught Signal ... cleaning up."
  cd $curdir
  rm -rf $tmpdir
  echo "Done cleanup ... quitting."
  if [ $exit_status -ne 0 ] ; then
    echo "*** ERROR in $0!!! ***"
  fi
  exit $exit_status
}

# repack
pakrs_float1 $curdir/$rs1 rs

# find FARE records
nrec1=`ggstat rs | grep 'FARE      1001' | head -1 | cut -c1-7 | awk '{printf "%10d",$1}'`
echo "nrec1=$nrec1"
nrec2=`echo $nrec1 | awk '{printf "%10d",$1+5}'`
echo "nrec2=$nrec2"
nrec1m1=`echo $nrec1 | awk '{printf "%10d",$1-1}'`
nrec2p1=`echo $nrec2 | awk '{printf "%10d",$1+1}'`

# split rs fils
echo "C*RCOPY            1$nrec1m1" | ccc rcopy rs rs.1
echo "C*RCOPY   $nrec1$nrec2" | ccc rcopy rs rs.2
echo "C*RCOPY   $nrec2p1 999999999" | ccc rcopy rs rs.3

# compile fix_fare program
cat > fix_fare.f <<eor
      PROGRAM FIX_FARE
      REAL FARE(18528,6)
      COMMON/ICOM/IBUF(8),IDAT(37056)
      DATA MAXX/37056/
C---------------------------------------------------------------------
      NFF=3
      CALL JCLPNT(NFF,1,2,6)
      REWIND 1
      REWIND 2
C
C     * READ THE NEXT PAIR OF FIELDS.
C
      DO K=1,6
        CALL GETFLD2(1,FARE(1,K),-1,-1,NC4TO8("FARE"),-1,IBUF,MAXX,OK)
        CALL PRTLAB(IBUF)
      ENDDO
      NWDS=IBUF(5)*IBUF(6)
      DO IJ=1,NWDS
        SUM=0.
        DO K=1,6
          SUM=SUM+FARE(IJ,K)
        ENDDO
        IF(ABS(SUM-1.).GT.1E-6)THEN
          J=IJ/IBUF(5)+1
          I=IJ-(J-1)*IBUF(5)
          WRITE(6,'(3(A,I5),A,F10.6)')' IJ=',IJ,' J=',J,' I=',I,
     +            ' FARE SUM=',SUM
          FARE(IJ,1)=1.
        ENDIF
      ENDDO
C
C     * SAVE THE ADJUSTED
C
      DO K=1,6
        IBUF(4)=1000+K
        CALL PUTFLD2(2,FARE(1,K),IBUF,MAXX)
      ENDDO
C---------------------------------------------------------------------
      END
eor
nogo float2 input=fix_fare.f a=fix_fare

# run fix_fare
fix_fare rs.2 rs.2.fixed

# reconstruct RS
cat rs.1 rs.2.fixed rs.3 > $curdir/$rs2


#! /bin/sh
 
#   Dec 03/08 - F.Majaess (Revised for sa/saiph, ib/dorval-ib, af/alef)
#   Oct 16/06 - F.Majaess (Revised for ma/maia, ns/naos)
#   Mar 01/06 - F.Majaess (Revised for rg/rigel)
#   Jan 25/05 - F.Majaess (Revised for "lxsrv,[lx,ltop][0-9][0-9]")
#   Jun 20/03 - F.Majaess (Revised for az/azur)
#   Feb 25/02 - F.Majaess (Revised for ya/yata)
#   Sep 14/00 - F.Majaess (Revised for tu/tsunami)
#   Jan 05/00 - F.Majaess (Added kz/kaze)
#   Jun 11/99 - F.Majaess (Removed "oldmod" support)
#   Jun 10/99 - F.Majaess (Avoid "proxy" connection)
#   Jan 21/99 - F.Majaess (Added yo/yonaka)
#   May 04/98 - F.Majaess (Removed o2/o2000-2)
#   Mar 23/98 - F.Majaess (Added o2/O2000-2)
#   Sep 16/07 - F.Majaess (Added sx/hiru)
#   Feb 10/97 - F.Majaess (Change pollux.cmc.doe.ca to pollux.cmc.ec.gc.ca)
#   Nov 04/96 - F.Majaess (Revised to honour and use "oldiag" environment 
#                          variable, when it's set by the user, in setting 
#                          the path to the standard diagnotic library to 
#                          use) 
#    Sep 11/96 - F.Majaess (Adjust path massaging for Victoria)
#    Mar 14/96 - F.Majaess (Added sx4)
#    Sep 27/95 - F.Majaess (Revise the path massaging for Victoria)
#    Jun 01/95 - F.Majaess (Replace cr01 by orion)
#    Apr 11/95 - F.Majaess (Modified to add Downsview as a Site_ID and
#                           replacing cidsv08 by cr01)
#    Feb 21/95 - F.Majaess
 
#id  rsubmit3- submit a local file to a local or remote site. 
 
#    AUTHOR  - F.Majaess
 
#hd  PURPOSE - rsubmit3 script is used to transfer "fn" from
#hd            local machine to remote server's "despath" 
#hd            directory and then submit the file to the 
#hd            appropriate nqs queue. 
 
#pr  PARAMETERS:
#pr
#pr    POSITIONAL     
#pr
#pr      fn = file containing submission job (filename may contain path
#pr           information)
#pr
#pr    PRIMARY
#pr
#pr      Note: As applicable, one of 'ha','pha','sp','psp','po','ca',
#pr            'mz', ... for CMC, 
#pr            or
#pr            'lxsrv[2,new]/lxwrk1/lxwrk2/lx01.../lxlp01.../ltop01...' for Victoria
#pr            is to be specified.
#pr
#pr     hr/hare          = switch to execute job on hare.
#pr
#pr     br/brooks        = switch to execute job on brooks.
#pr
#pr     p1/ppp1          = switch to execute job on ppp1.
#pr
#pr     p2/ppp2          = switch to execute job on ppp2.
#pr
#pr     ha               = switch to execute job on the IBM (hadar).
#pr     pha              = switch to execute job on the IBM (hadar) targeting 
#pr                        "preemptable" class.
#pr     sp               = switch to execute job on the IBM (spica).
#pr     psp              = switch to execute job on the IBM (spica) targeting
#pr                        "preemptable" class.
#pr     po|pollux        = switch to execute job on the Linux (pollux).
#pr     ca|castor        = switch to execute job on the Linux (castor).
#pr     mz|mez           = switch to execute job on the Linux (mez).
#pr
#pr     lxsrv/lxsrv2/lxsrvnew/
#pr     lxwrk1/lxwrk2/
#pr     lx[0-9][0-9]/
#pr     lxlp[0-9][0-9]/
#pr     ltop[0-9][0-9]   = switch to submit/run job on lxsrv/lxsrv2,lxsrvnew,
#pr                        lxwrk1/lxwrk2/lx[0-9][0-9]/lxlp[0-9][0-9]/ltop[0-9][0-9]
#pr                        Linux based platform in Victoria.
#pr
#pr     despath= subdirectory relative to $HOME in which the transfered 
#pr              job get temporarily stored at remote server 
#pr              (=.queue).
#pr
#pr    PRIMARY/SECONDARY
#pr
#pr     script = switch to generate a copy of the pre-processed job
#pr              script (i.e the next job in the string) without sending
#pr              the job to NQS (diagnostic jobs only)
#pr     resume = switch to resume a string
#pr        log = switch to force return of printout via NQS
#pr
#pr   continue = switch to continue a string (used only in endjcl.cdk)
#pr
#pr   delaymin = optional switch valid only for job targeting one of AIX 
#pr              Power7 clusters at CMC in Dorval. If specified,it signals
#pr              to defer start execution of the job by "delaymin" time
#pr              in minutes.
#pr
#pr     premtn = optional switch which allows targeting "preemptable" instead
#pr              of "development" class on the "Production" P7 cluster where,
#pr              the job will be subject to pre-emption. 
#pr              =on/yes; option enabled.
 
#ex  EXAMPLE: 
#ex
#ex          rsubmit3 ha myjob
#ex
#ex    The above example submits 'myjob' in the current directory to Hadar.
#ex
#ex          rsubmit3 myjob psp
#ex          rsubmit3 myjob premtn sp
#ex
#ex    Either of the above 2 will result in submitting 'myjob' to Spica 
#ex    targeting "preemptable" class where it'll be subject to "preemption"
#ex    provided Spica is running in "Production" cluster mode.
#ex
#ex          rsubmit3 despath=work lxsrv myjob
#ex
#ex    Assuming the script is invoked from other than Victoria's platforms;
#ex    the above example transfers 'myjob' into '$HOME/work' in Victoria
#ex    and submits the job to run in the background on "lxsrv" server.
#ex
#ex
#ex     rsubmit3 jobfil delaymin=30 ha
#ex
#ex     The above example results in submitting the job to Hadar with
#ex     deferred start execution by 30 minutes.
 
if [ -z "$SYSTEM_SETUP" -a -z "$CCRNSRC" -a -d "/users/tor/acrn/src/generic/."  ] ; then [ -s "/users/tor/acrn/src/generic/cccma_setup_profile" ] && . /users/tor/acrn/src/generic/cccma_setup_profile || : ; fi
if [ -z "$SYSTEM_SETUP" -a -z "$CCRNSRC" -a -d /home/scrd101/generic/. ] ; then [ -s /home/scrd101/generic/sc_cccma_setup_profile ] && . /home/scrd101/generic/sc_cccma_setup_profile || : ; fi
#  * Obtain the submission file name and any specified option.

for arg in $@
do
  case $arg in
       -*) set $arg ; set_opt=$arg                          ;;
      *=*) eval $arg                                        ;;
   hr|hare|br|brooks|p1|ppp1|p2|ppp2) mdest=${mdest:=$arg} ;;
   ha|hadar|sp|spica|po|pollux|ca|castor|mz|mez) mdest=${mdest:=$arg} ;;
        lxsrv|lxsrv2|lxsrvnew|lxwrk1|lxwrk2|lx[0-9][0-9]|ltop[0-9][0-9]) mdest=${mdest:=$arg} ;;
        lxlp[0-9][0-9]) mdest=${mdest:=$arg} ;;
        pha) mdest='hadar' ; premtn='yes'         ;;
        psp) mdest='spica' ; premtn='yes'         ;;
      resume) resume='resume'                               ;;
    continue) continue='continue'                           ;;
         log) log='log'                                     ;;
      script) script='script'                               ;;
        *) fn=${fn:=$arg}
  esac
done
 
#  * Set the defaults.

eval "despath=${despath='.queue'}"
eval "continue=${continue='  '}"
eval "script=${script='  '}"
eval "resume=${resume='  '}"
eval "log=${log='  '}"
if [ -n "$oldiag" ] ; then
  oldiag="oldiag=$oldiag"
fi
if [ -n "$topdog" ] ; then
  topdog="topdog=$topdog"
fi
if [ -n "$premtn" ] ; then
  premtn="premtn=$premtn"
fi
if [ -n "$delaymin" ] ; then
  delaymin="delaymin=$delaymin"
fi

#  * Prompt for a destination if none was specified.

while [ -z "$mdest" ]
do
  echo "please enter a destination: > \\c"
  read tdest
  case $tdest in
   hr|hare|br|brooks|p1|ppp1|p2|ppp2) mdest=$tdest ;;
  #ma|maia|za|zeta|al|algol|ha|hadar|sp|spica|sa|saiph|ib|dorval-ib|pollux)  mdest=$tdest ;;
   ha|hadar|sp|spica|po|pollux|ca|castor|mz|mez)  mdest=$tdest ;;
         lxsrv|lxsrv2|lxsrvnew|lxwrk1|lxwrk2|lx[0-9][0-9]|ltop[0-9][0-9]) mdest=$tdest ;;
         lxlp[0-9][0-9]) mdest=$tdest ;;
       *) echo "illegal destination $tdest ! "      ;;
  esac
done
## if [ -n "$mdest" -a "$mdest" = 'orion' ] ; then 
##  echo "" ; echo "rsubmit3: Error; $mdest destination is no longer supported!"
##  tty -s && InTrCtv='true' || InTrCtv='false'
##  if [ "$InTrCtv" = 'false' ] ; then 
##   touch haltit
##   ( echo "" ; echo "rsubmit3: Error; $mdest destination is no longer supported!") >> haltit
##  fi
##  exit 2
## fi

# * Make sure the site and destinations are both valid
# * and setup required variables.

# RSH='rsh'
# if [ "$SITE_ID" = 'Victoria' -a  `echo $HOSTID | cut -c 1-4` = 'ltop' -a `echo $HOSTID | cut -c 1-4` != 'lxlp' ] ; then
  RSH='ssh'
# fi

if [ "$SITE_ID" = 'Victoria' -o "$SITE_ID" = 'Dorval' -o "$SITE_ID" = 'DrvlSC' ] ; then
  if [ "$SITE_ID" = 'Victoria' ] ; then
  #if [ "$OS" = 'AIX' ] ; then
    # ftpmid='ftp1.cccma.ec.gc.ca'
  # ftpmid='ax01.cccma.ec.gc.ca'
  #else
    ftpmid="$mdest.cccma.ec.gc.ca"
  #fi
  fi
  case $mdest in
       ha|hadar|sp|spica|po|pollux|ca|castor|mz|mez)  if [ "$SITE_ID" != 'Dorval' ] 
                                       then
#                                       if [ `echo $HOSTID | cut -c 1-4` != 'ltop' -a `echo $HOSTID | cut -c 1-4` != 'lxlp' ]
#                                       then
#                                        RSH='ssh'
#                                       fi
                                        # rmachine='pollux.cmc.ec.gc.ca' 
                                        case $mdest in
                                        #af|alef) CMCFEDEST='alef';
                                        #              RSH='ssh'
                                        #              rmachine=${rmachine:="${CMCFEDEST}.cmc.ec.gc.ca"} ;;
                                         ca|castor) CMCFEDEST='castor';
                                                        RSH='ssh'
                                                       rmachine=${rmachine:="${CMCFEDEST}.cmc.ec.gc.ca"} ;;
                                            ma|mez) CMCFEDEST='mez';
                                                        RSH='ssh'
                                                       rmachine=${rmachine:="${CMCFEDEST}.cmc.ec.gc.ca"} ;;
                                         #po|pollux) CMCFEDEST='pollux';
                                                    *) CMCFEDEST='pollux';
                                                      #if [ "$SITE_ID" = 'Victoria' ] ; then
                                                        RSH='ssh'
                                                      #fi ;
                                                       rmachine=${rmachine:="${CMCFEDEST}.cmc.ec.gc.ca"} ;;
                                        #           *) CMCFEDEST=${CMCFEDEST:='pollux'};
                                        #              rmachine=${rmachine:="${CMCFEDEST}.cmc.ec.gc.ca"} ;;
                                        esac
                                       fi ;;
       hr|hare|br|brooks|p1|ppp1|p2|ppp2) if [ "$SITE_ID" != 'DrvlSC' ] 
                                       then
                                        case $mdest in
                                         p2|ppp2) CMCFEDEST='ppp2';
                                                        RSH='ssh'
                                                       rmachine=${rmachine:="${CMCFEDEST}.science.gc.ca"} ;;
                                                    *) CMCFEDEST='ppp1';
                                                   #*) CMCFEDEST='hare';
                                                        RSH='ssh'
                                                       rmachine=${rmachine:="${CMCFEDEST}.science.gc.ca"} ;;
                                        esac
                                       fi ;;
   lxsrv|lxsrv2|lxsrvnew|lxwrk1|lxwrk2|lx[0-9][0-9]|lxlp[0-9][0-9]|ltop[0-9][0-9]) if [ "$SITE_ID" != 'Victoria' -o \( "$SITE_ID" = 'Victoria' -a "$mdest" != "$HOSTID" \) ]
                                       then
                                        RSH='ssh'
                                        ftpmid="$mdest.cccma.ec.gc.ca"
                                        rmachine="$mdest.cccma.ec.gc.ca"    
                                       fi ;;
       *) echo "illegal destination $tdest ! "          
          exit 2 
  esac
else
  echo " SITE_ID environment must be set to supported sites Victoria or Dorval, sorry"
  exit 3
fi

#
# * Make sure submission job file is specified
#

if [ "$resume" != "resume" -a "$continue" != "continue" ] ; then

 while [ -z "$fn" ] ; do
  echo "please enter [path/]filename ? > \\c"
  read fn
 done

#  * Set default path to the submission job (if necessary)

 tarpath=`expr $fn : '\(.*\)/'`
 if [ "$tarpath" = '..' -o "$tarpath" = '.' ] ; then Cwd=`pwd` ; tarpath=`expr ${Cwd}/${fn} : '\(.*\)/'` ; fi
 tarpath=${tarpath:=`pwd`}
 target=`expr //$fn : '.*/\(.*\)'`
 while [ -z "$target" ] ; do
  echo "please enter filename ? > \\c"
  read target
 done

#  * Check if the submission job file exists, readable and non-empty
#  * otherwise, issue an abort message and exit.

 if [ "$SITE_ID" = 'Victoria' -a `echo $HOSTID | cut -c 1-4` != 'ltop' -a `echo $HOSTID | cut -c 1-4` != 'lxlp' ] ; then
   eval tarpath=`echo $tarpath | sed -n -e "s/\/HOME\/$USR\/$USR/\/home\/$USR/" -e '1,\$p' `
   eval tarpath=`echo $tarpath | sed -n -e "s/\/HOME\//\/home\//" -e '1,\$p' ` 
#  eval tarpath=`echo $tarpath | sed -n -e "s/^\/tmp_mnt\/home\/$USR\/$USR/\/home\/$USR/" -e "s/^\/tmp_mnt\/home\/$USER/\/home/" -e "s/^\/nfs\/cs..//" -e "s/^\/tmp_mnt\/net\/cs..-fddinet//" -e "s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" -e "s/^\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" -e '1,\$p' ` 
  tarpath=`echo $tarpath | sed "
    s/^\/tmp_mnt\/home.*\/$USR/\/home\/$USR/
    s/^\/tmp_mnt\/home\/$USER/\/home/
    s/^\/nfs\/cs..//
    s/^\/nfs\/sp..s//
    /^\/tmp_mnt\/net\/cs..-fddinet/{
     s/\/$USR\/$USR/\/$USR/g
     s/\/$USR\/$USR/\/$USR/g
     s/^\/tmp_mnt\/net\/cs..-fddinet//
                                   }
    /^\/tmp_mnt\/net\/sp..s/{
     s/\/$USR\/$USR/\/$USR/g
     s/\/$USR\/$USR/\/$USR/g
     s/^\/tmp_mnt\/net\/sp..s//
                                   }
    s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/
    s/^\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" `
 fi

 if [ ! -s "$tarpath/$target" ] ; then
  eval "echo $tarpath/$target is not a valid filename !"
  exit 1
 fi
 fn=$target
 target="$tarpath/$target"
 
else

 while [ -z "$fn" ] ; do
  echo "please enter filename ? > \\c"
  read fn
 done
 target=$fn

fi 
if [ -n "$JHOME" ] ; then
 jhome="JHOME=$JHOME" 
fi
if [ -n "$reset_jhome" ] ; then
 reset_jhome="reset_jhome=$reset_jhome" 
fi

# * Transfer (if necessary) and submit the job.

if [ "$resume" = "resume" -o "$continue" = "continue" ] ; then
 
#  * Job recovering mode, just resume running the job 

 if [ -n "$rmachine" ] ; then
  #$RSH $rmachine -n "submit3 $set_opt $target $mdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome "
   strng='. ~acrnsrc/generic/cccma_setup_profile ; '"submit3 $set_opt $target $mdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome"
   # $RSH $rmachine -n " echo $strng | /bin/bash -"
   echo "echo $strng" | $RSH -T $rmachine "/bin/bash -l -"
 else
   submit3 $set_opt $target $mdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome 
 fi

else

#  * No recovering mode

 if [ -n "$rmachine" ] ; then

#  * Job is to be transfered and submitted offsite 

  Pid=$$ &&

  Tmpfl="tmp.rsubmit3.$fn.$Pid" &&
# Tmdest=${mdest:+"mdest=$mdest"}
  Tmdest=$mdest &&
  Cwd=`pwd` &&

  if [ "$SITE_ID" = 'Victoria' ] ; then

  rmthome=`$RSH $rmachine -n pwd` 

#  * Transfer a temporary copy of the submission job from Victoria to Dorval

   if [ `echo $HOSTID | cut -c 1-4` != 'ltop' -a `echo $HOSTID | cut -c 1-4` != 'lxlp' ] ; then
   eval Cwd=`echo $Cwd | sed -n -e "s/\/HOME\/$USR\/$USR/\/home\/$USR/" -e '1,\$p' ` &&
   eval Cwd=`echo $Cwd | sed -n -e "s/\/HOME\//\/home\//" -e '1,\$p' ` &&
#  eval Cwd=`echo $Cwd | sed -n -e "s/^\/tmp_mnt\/home\/$USR\/$USR/\/home\/$USR/" -e "s/^\/tmp_mnt\/home\/$USER/\/home/" -e "s/^\/nfs\/cs..//" -e "s/^\/tmp_mnt\/net\/cs..-fddinet//" -e '1,\$p' ` &&
  Cwd=`echo $Cwd | sed "
    s/^\/tmp_mnt\/home.*\/$USR/\/home\/$USR/
    s/^\/tmp_mnt\/home\/$USER/\/home/
    s/^\/nfs\/cs..//
    s/^\/nfs\/sp..s//
    /^\/tmp_mnt\/net\/cs..-fddinet/{
     s/\/$USR\/$USR/\/$USR/g
     s/\/$USR\/$USR/\/$USR/g
     s/^\/tmp_mnt\/net\/cs..-fddinet//
                                   }
    /^\/tmp_mnt\/net\/sp..s/{
     s/\/$USR\/$USR/\/$USR/g
     s/\/$USR\/$USR/\/$USR/g
     s/^\/tmp_mnt\/net\/sp..s//
                                   }
    s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/
    s/^\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" ` &&

#  ( /usr/ucb/ftp -i -v $ftpmid << wooftp )  &&
##    ( ftp -i -v $ftpmid << wooftp )  &&
##     cd $Cwd
##     proxy open $rmachine
##     proxy ascii
##     proxy cd $despath
##     proxy get $target $Tmpfl
##     quit
## wooftp

##    if [ "$rmachine" = 'alef.cmc.ec.gc.ca' ] ; then
##     scp $target $rmachine:$rmthome/$despath/$Tmpfl 
##    elif [ "$rmachine" = 'dorval-ib.cmc.ec.gc.ca' ] ; then
##     scp $target $rmachine:$rmthome/$despath/$Tmpfl 
##    elif [ "$RSH" = 'ssh' ] ; then
##     scp $target $rmachine:$rmthome/$despath/$Tmpfl
##    else
##    ( ftp -i -v $rmachine << wooftp )  
##     ascii
##     cd $despath
##     put $target $Tmpfl
##     quit
## wooftp
##     fi &&
##     :
    scp $target $rmachine:$rmthome/$despath/$Tmpfl
   else
    scp $target $rmachine:$rmthome/$despath/$Tmpfl
    # scp -o NoneSwitch=yes $target $rmachine:$rmthome/$despath/$Tmpfl
   fi 
 
  else

#  * Transfer a temporary copy of the submission job from Dorval to Victoria. 

 

  rmthome=`$RSH $rmachine -n pwd`  &&
  #if [ "$rmachine" = 'ftp1.cccma.ec.gc.ca' -o "$rmachine" = 'ax01.cccma.ec.gc.ca' ] ; then
   if [ "$rmachine" = 'ftp1.cccma.ec.gc.ca' ] ; then
    if [ `echo $HOSTID | cut -c 1-4` != 'ltop' -a `echo $HOSTID | cut -c 1-4` != 'lxlp' ] ; then
    eval rmthome=`echo $rmthome | sed -n -e "s/\/HOME\/$USR\/$USR/\/home\/$USR/" -e '1,\$p' ` &&
    eval rmthome=`echo $rmthome | sed -n -e "s/\/HOME\//\/home\//" -e '1,\$p' ` &&
#   eval rmthome=`echo $rmthome | sed -n -e "s/^\/tmp_mnt\/home\/$USR\/$USR/\/home\/$USR/" -e "s/^\/tmp_mnt\/home\/$USER/\/home/" -e "s/^\/nfs\/cs..//" -e "s/^\/tmp_mnt\/net\/cs..-fddinet//" -e '1,\$p' ` 

    rmthome=`echo $rmthome | sed "
     s/^\/tmp_mnt\/home.*\/$USR/\/home\/$USR/
     s/^\/tmp_mnt\/home\/$USER/\/home/
     s/^\/nfs\/cs..//
     s/^\/nfs\/sp..s//
     /^\/tmp_mnt\/net\/cs..-fddinet/{
      s/\/$USR\/$USR/\/$USR/g
      s/\/$USR\/$USR/\/$USR/g
      s/^\/tmp_mnt\/net\/cs..-fddinet//
                                    }
    /^\/tmp_mnt\/net\/sp..s/{
     s/\/$USR\/$USR/\/$USR/g
     s/\/$USR\/$USR/\/$USR/g
     s/^\/tmp_mnt\/net\/sp..s//
                                   }
     s/^\/tmp_mnt\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/
     s/^\/net\/ss..\/home\/gjb/\/home\/rgb\/sunhome/" `
    fi &&
##     if [ "$SITE_ID" = 'Dorval' -a \( "$OS" = 'Linux' -o "$OS" = 'IRIX64' \) ] ; then
##      scp $target $ftpmid:$rmthome/$despath/$Tmpfl
##     else
## #    ( /usr/ucb/ftp -i -v $ftpmid << wooftp ) &&
##      ( ftp -i -v $ftpmid << wooftp ) 
##      cd $despath
##      ascii
##      put $target $Tmpfl
##      quit
## wooftp
##     fi
    scp $target $ftpmid:$rmthome/$despath/$Tmpfl
   fi
  fi &&


#  * Submit the job on the remote machine and cleanup temporary file

  # $RSH  $rmachine -n "cd $despath ; submit3 $set_opt $rmthome/$despath/$Tmpfl $Tmdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome "  &&
  #  $RSH $rmachine -n " echo $strng | /bin/bash -"
   # strng='. ~acrnsrc/generic/cccma_setup_profile ; '"cd $despath ; submit3 $set_opt $rmthome/$despath/$Tmpfl $Tmdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome " &&
   # $RSH $rmachine -n " echo $strng | /bin/bash -" &&
   echo "echo $strng" | $RSH -T $rmachine "/bin/bash -l -" &&
#  echo " $target file is believed to be submitted " &&
  $RSH  $rmachine -n "\rm -rf $rmthome/$despath/$Tmpfl "  

 else

#   * Job is to be submitted on the local site

    submit3 $set_opt $target $mdest $script $continue $resume $log $oldiag $topdog $premtn $delaymin $jhome $reset_jhome 

 fi
fi &&
exit 0 ||
echo " problem in transfering/submitting $target " &&
exit 4
